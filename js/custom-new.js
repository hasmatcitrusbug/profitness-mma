$(window).load(function(){
    var $container = $('.gallery-grid-row');
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    });

    $('.gallery-filters a').click(function(){
        $('.portfolioFilter .is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');

        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        return false;
    }); 
});

